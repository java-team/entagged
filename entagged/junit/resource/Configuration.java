/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.resource;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Loads the config.properties in the same package and provides Typed acess to
 * the values.
 * 
 * @author Christian Laireiter
 */
public class Configuration {

    /**
     * This field can be used to store results of these methods, if parsing the
     * constants will take much time if called often.
     */
    private final static HashMap buffer = new HashMap();
    /**
     * Stores the properties file.
     */
    private final static Properties properties;

    /**
     * Loads the config.properties.
     */
    static {
        try {
            InputStream is = Configuration.class.getClassLoader()
                    .getResourceAsStream(
                            "entagged/junit/resource/config.properties");
            properties = new Properties();
            properties.load(is);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Cannot initialize main configuration.", e);
        }
    }

    /**
     * This method reads the Strng value for the named property and tries to
     * convert it to a boolean.
     * 
     * @param name
     *                  Name of the Property
     * @return Boolean interpretation for the String.
     */
    public static boolean getBoolean(String name) {
        String value = getProperty(name);
        return Boolean.valueOf(value).booleanValue();
    }

    /**
     * This method will call {@link #getStringArray(String)}and convert the
     * values into characters. <br>
     * Value->Result: <br>
     * "\n" -> Java \n char <br>
     * 0x0A -> (char)10 <br>
     * 
     * @param name
     *                  propertyname
     * @return interpreted values.
     */
    public static char[] getCharArray(String name) {
        if (buffer.containsKey("charArray:" + name)) {
            return (char[]) buffer.get("charArray:" + name);
        }
        String[] definitions = getStringArray(name);
        char[] result = new char[definitions.length];
        for (int i = 0; i < definitions.length; i++) {
            String current = definitions[i];
            if (current.trim().startsWith("0x")) {
                result[i] = (char) Integer.parseInt(current.substring(2), 16);
            } else if (current.trim().equals("\n")) {
                result[i] = '\n';
            } else if (current.length() == 1) {
                result[i] = current.charAt(0);
            }
        }
        buffer.put("charArray:" + name, result);
        return result;
    }

    /**
     * This method will interpret the property as a file path.
     * 
     * @param name
     *                  propertyname
     * @return File instance
     */
    public static File getFile(String name) {
        return new File(getProperty(name));
    }

    /**
     * This method reads the String value for the named property and tries to
     * convert it to an integer.
     * 
     * @param name
     *                  Name of the Property.
     * @return Integer interpretation for the String.
     */
    public static int getInteger(String name) {
        String value = getProperty(name);
        return Integer.valueOf(value).intValue();
    }

    /**
     * Simply forwards request to {@link #properties}.
     * 
     * @param name
     *                  Name of the Property
     * @return Stored Value.
     */
    public static String getProperty(String name) {
        String result = properties.getProperty(name);
        if (result == null) {
            throw new Error("Property \"" + name + "\" not defined.");
        }
        return result;
    }

    /**
     * This method will read the given property ans split it by ";".
     * 
     * @param name
     *                  name of the property
     * @return splitted values.
     */
    public static String[] getStringArray(String name) {
        return getProperty(name).split(";");
    }

}