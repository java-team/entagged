/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.audioformats;

import java.io.File;
import java.util.Arrays;

import junit.framework.TestCase;
import entagged.audioformats.AudioFile;
import entagged.audioformats.AudioFileIO;
import entagged.audioformats.Tag;
import entagged.audioformats.ogg.OggTag;
import entagged.junit.Utils;
import entagged.junit.resource.Configuration;

/**
 * This Testcase tests the access for a audio file using entagged audioformats.
 * 
 * @author Christian Laireiter
 */
public class FileFormatAccessTest extends TestCase {

	/**
	 * This field will store the AudioFile instance if successful.
	 */
	private AudioFile audioFile;

	/**
	 * This field stores the file on which the tests should be run.
	 */
	private File file;

	/**
	 * Stores the name for the current test. <br>
	 */
	private final String name;

	/**
	 * Creates a test for the given file.
	 * 
	 * @param toTest
	 *            The file which should be tested.
	 * @throws Exception
	 *             Any exception occuring
	 */
	public FileFormatAccessTest(File toTest) throws Exception {
		this.name = "FileFormatTest on " + toTest.getName();
		this.file = toTest;
	}

	/**
	 * This method will check if the comment was properly written.
	 * 
	 * @param original
	 *            The String which was written by the test class.
	 * @param read
	 *            The String which was read after writing.
	 */
	public void checkComment(String original, String read) {
		int index = Arrays.binarySearch(Configuration
				.getStringArray("junit.format.multiline.types"), audioFile
				.getName().substring(audioFile.getName().lastIndexOf(".") + 1));
		assertFalse("Format Comment character not configured: "
				+ audioFile.getEncodingType(), index < 0);
		char newLineChar = Configuration
				.getCharArray("junit.format.multiline.newlinechar")[index];
		original = original.replaceAll("\n", "" + newLineChar);
		assertEquals("Comment not property written or read.", original, read);
	}

	/**
	 * (overridden)
	 * 
	 * @see junit.framework.TestCase#getName()
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * (overridden)
	 * 
	 * @see junit.framework.TestCase#runTest()
	 */
	protected void runTest() throws Throwable {
		this.audioFile = AudioFileIO.read(this.file);
		Tag tag = audioFile.getTag();
		tag.setAlbum("Album");
		tag.setArtist("Artist");
		tag.setComment("Multiline\nComment");
		tag.setGenre("Genre");
		tag.setTitle("Title");
		tag.setTrack("Track");
		tag.setYear("Year");
		AudioFileIO.write(audioFile);
		audioFile = AudioFileIO.read(this.file);
		tag = audioFile.getTag();
		assertEquals("Written album", "Album", tag.getFirstAlbum());
		assertEquals("Written artist", "Artist", tag.getFirstArtist());
		checkComment("Multiline\nComment", tag.getFirstComment());
		assertEquals("Written genre", "Genre", tag.getFirstGenre());
		assertEquals("Written title", "Title", tag.getFirstTitle());
		assertEquals("Written track", "Track", tag.getFirstTrack());
		assertEquals("Written Year", "Year", tag.getFirstYear());
		if (audioFile.getTag() instanceof OggTag
				&& Configuration.getBoolean("ogg.extensive.enabled")
				&& (audioFile.getEncodingType() == null || (audioFile
						.getEncodingType().toUpperCase().indexOf("FLAC") == -1))) {
			testOggInfo(this.file);
		}
	}

	/**
	 * (overridden)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		assertTrue("Cannot read file: " + file.getName(), this.file.exists()
				&& this.file.isFile() && this.file.canRead());
	}

	/**
	 * This method will call "ogginfo" and tests if it still reads the file.
	 * 
	 * @param file2
	 *            Ogg/Vorbis file to check.
	 */
	private void testOggInfo(File file2) {
		try {
			Process process = Runtime.getRuntime().exec(
					"ogginfo " + file2.getAbsolutePath());
			long start = System.currentTimeMillis();
			int exitValue = -1;
			int failedTimeout = Configuration
					.getInteger("ogg.extensive.timeout");
			StringBuffer output = new StringBuffer();
			while (exitValue == -1) {
				try {
					output.append(Utils
							.readFromStream(process.getInputStream()));
					if (output.indexOf("Warning:") >= 0) {
						process.destroy();
						exitValue = 2;
					} else
						exitValue = process.exitValue();
				} catch (Exception e) {
					// e.printStackTrace();
					if ((System.currentTimeMillis() - start) > failedTimeout) {
						exitValue = 1;
						System.err.println("Timeout");
						break;
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
			assertEquals("Ogginfo returned some error.", 0, exitValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}