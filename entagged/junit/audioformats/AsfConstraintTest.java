/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.audioformats;

import java.io.File;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.util.Arrays;

import junit.framework.Assert;
import junit.framework.TestCase;
import entagged.audioformats.asf.data.AsfHeader;
import entagged.audioformats.asf.data.ContentDescription;
import entagged.audioformats.asf.data.ContentDescriptor;
import entagged.audioformats.asf.io.AsfHeaderReader;

/**
 * This class should test the constaints of the ASF implementation. <br>
 * For example, no string must exceed a length of 65535 bytes.
 * 
 * @author Christian Laireiter (liree)
 */
public class AsfConstraintTest extends TestCase {

    /**
     * The asf file which is used for the implementation verfification.
     */
    private File file;

    /**
     * Creates an instance, that will use the given file.
     * 
     * @param toTest
     *                   ASF container.
     */
    public AsfConstraintTest(File toTest) {
        super("ASF Test on : " + toTest.getAbsolutePath());
        this.file = toTest;
    }

    /**
     * This method will create a String of the specified <code>length</code>
     * and where each character is the given <code>toFillWith</code>.
     * 
     * @param toFillWith
     *                   The character to fill the string with.
     * @param length
     *                   the length of the string.
     * @return The created String.
     */
    private String createString(char toFillWith, int length) {
        char[] result = new char[length];
        Arrays.fill(result, toFillWith);
        return new String(result);
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#runTest()
     */
    protected void runTest() throws Throwable {
        AsfHeader header = AsfHeaderReader.readHeader(new RandomAccessFile(
                this.file, "r"));
        String okString = createString('a', 100);
        String errorString = createString('a', 65536);
        if (header.getContentDescription() != null) {
            int errors = testContentDescription(header, okString);
            assertTrue(errors == 0);
            errors = testContentDescription(header, errorString);
            assertTrue(errors == 5);
        }
        ContentDescriptor desc;
        try {
            desc = new ContentDescriptor(errorString,
                    ContentDescriptor.TYPE_STRING);
            Assert
                    .fail("Construction shouldn't be possible with such a large name");
        } catch (Exception e) {
            // Here is all OK
        }
        desc = new ContentDescriptor(okString, ContentDescriptor.TYPE_STRING);
        try {
            desc.setStringValue(errorString);
            Assert.fail("Value is too long but accepted");
        } catch (Exception e) {
            // Here is all OK
        }
        desc.setStringValue(okString);

        assertTrue(header.getFileHeader().getFileSize().equals(
                BigInteger.valueOf(this.file.length())));
    }

    /**
     * This method will test the correct implementation of the
     * {@link entagged.audioformats.asf.data.ContentDescription}implementation.
     * <br>
     * 
     * @param header
     *                   Instance for accesses.
     * @param toTest
     *                   This string will be set to each known field.
     * @return The number of errors occured.
     */
    private int testContentDescription(AsfHeader header, String toTest) {
        int errorCount = 0;
        ContentDescription desc = header.getContentDescription();
        try {
            desc.setAuthor(toTest);
        } catch (Exception e) {
            errorCount++;
        }
        try {
            desc.setComment(toTest);
        } catch (Exception e) {
            errorCount++;
        }
        try {
            desc.setCopyRight(toTest);
        } catch (Exception e) {
            errorCount++;
        }
        try {
            desc.setRating(toTest);
        } catch (Exception e) {
            errorCount++;
        }
        try {
            desc.setTitle(toTest);
        } catch (Exception e) {
            errorCount++;
        }
        return errorCount;
    }

}