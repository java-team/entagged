/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.audioformats;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import entagged.audioformats.AudioFileFilter;
import entagged.junit.Utils;
import entagged.junit.resource.Configuration;

/**
 * This class tests the media files within a configured directory.
 * 
 * @author Christian Laireiter
 */
public class DirectoryTest extends TestSuite {

    /**
     * The first wma file whithin testing directory will be used to make this
     * test.
     */
    private AsfConstraintTest asfTest = null;

    /**
     * Entry point for JUnit
     * 
     * @return A Testsuite
     * @throws Throwable
     *                    anything
     */
    public static Test suite() throws Throwable {
        final DirectoryTest suite = new DirectoryTest();
        suite.addTest(new TestCase("Delete") {
            protected void runTest() throws Throwable {
                Assert.assertTrue("Delete copy "
                        + suite.directory.getAbsolutePath(), Utils
                        .deleteDirectory(suite.directory));
            }
        });
        return suite;
    }

    /**
     * This field stores all {@link File}objects of the copies of the content
     * of {@link #testDirectory}.<br>
     */
    private ArrayList copies = new ArrayList();

    /**
     * File instance of {@link #testDirectory}after {@link #setUp()}
     */
    protected File directory;

    public DirectoryTest() throws Exception {
        super();
        setUp();
    }

    public DirectoryTest(String name) throws Exception {
        super(name);
        setUp();
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#getName()
     */
    public String getName() {
        return "DirectoryFormatTests";
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        File testDirectory = Configuration.getFile("junit.format.directory");
        this.directory = Utils.createCopyOfDirectory(testDirectory);
        setName("DirecotryTest on " + testDirectory.getAbsolutePath());
        if (!(directory.exists() && directory.isDirectory() && directory
                .canRead())) {
            throw new AssertionFailedError(
                    "config.properties->junit.formats.directory should be a readable directory. ");
        }
        File[] files = directory.listFiles(new AudioFileFilter());
        for (int i = 0; i < files.length; i++) {
            File current = files[i];
            if (current.isFile() && current.canRead()) {
                File copy = Utils.createCopy(current);
                this.copies.add(copy);
                if (asfTest == null && copy.getName().endsWith("wma")) {
                    this.asfTest = new AsfConstraintTest(copy);
                    this.addTest(asfTest);
                }
                FileFormatAccessTest ffat = new FileFormatAccessTest(copy);
                addTest(ffat);
            }
        }
        System.out.println("Setup");
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        Iterator iterator = copies.iterator();
        while (iterator.hasNext()) {
            File current = (File) iterator.next();
            current.delete();
        }
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestSuite#run(junit.framework.TestResult)
     */
    public void run(TestResult result) {
        super.run(result);
        try {
            tearDown();
        } catch (Exception e) {
            e.printStackTrace();
            result.addError(this, e);
        }
    }
}