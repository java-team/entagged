/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.cli;

import java.io.File;

import junit.framework.TestCase;
import entagged.junit.resource.Configuration;

/**
 * Tests the call of {@link entagged.cli.ListingGenerator}.
 * 
 * @author Christian Laireiter
 */
abstract public class CliBase extends TestCase {

    /**
     * The testsuite.
     */
    protected CliSuite parent;

    /**
     * The file to which the report should be written.
     */
    protected File dst;

    /**
     * Creates an instance.
     * 
     * @param suite
     *                  Suite
     * @param name
     */
    public CliBase(CliSuite suite, String name) {
        super(name);
        this.parent = suite;
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        this.dst = new File(Configuration.getFile("junit.format.directory"),
                "reporttarget.txt");
        assertTrue(dst.createNewFile());
    }
    
    /** (overridden)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        assertTrue(this.dst.delete());
    }
}