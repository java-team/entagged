/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import entagged.cli.XslTransformer;
import entagged.listing.xml.TransformTarget;

/**
 * This test will perform the transformation using
 * {@link entagged.cli.XslTransformer#process(InputStream, OutputStream, TransformTarget)};
 * <br>
 * 
 * @author Christian Laireiter
 */
public class XslDirectTest extends XslCliTest {

    /**
     * Creates the test
     * 
     * @param target
     *                  What
     * @param report
     *                  source
     */
    public XslDirectTest(TransformTarget target, File report) {
        super(target, report);
        this.setName("Xsl Direct access with: " + target.getType() + " in "
                + target.getLanguage());
    }

    /**
     * (overridden)
     * 
     * @see entagged.junit.cli.XslCliTest#runTest()
     */
    protected void runTest() throws Throwable {
    	assertFalse(super.transformed.exists());
    	FileInputStream fis = new FileInputStream(super.reportFile);
        FileOutputStream fos = new FileOutputStream(super.transformed);
        XslTransformer.process(fis, fos, super.transformTarget);
        fos.close();
        assertTrue(super.transformed.exists() && super.transformed.canRead());
        assertTrue(super.transformed.length() > 0);
    }

}