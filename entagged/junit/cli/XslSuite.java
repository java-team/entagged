/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.cli;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import entagged.cli.XmlListingGenerator;
import entagged.cli.XslTransformer;
import entagged.junit.resource.Configuration;
import entagged.listing.xml.TransformTarget;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * This suite will get all configured Transformations and create a test for each
 * of them.
 * 
 * @author Christian Laireiter
 */
public class XslSuite extends TestSuite {

    /**
     * Creates a test suite.
     * 
     * @return test
     */
    public static Test suite() {
        return new XslSuite();
    }

    /**
     * Creates and initializes the suite.
     *
     */
    public XslSuite() {
        super("Xsl test suite");
        initialize();
    }

    /**
     * Get all Transformation targets and create Tests.
     */
    private void initialize() {
        File directory = Configuration.getFile("junit.format.directory");
        File report = new File(directory, "report.xml");
        XmlListingGenerator.main(new String[] { "-v",
                directory.getAbsolutePath(), report.getAbsolutePath() });
        Assert.assertTrue(report.length() > 0);
        Collection coll = XslTransformer.getTransformTargets();
        Iterator it = coll.iterator();
        ArrayList clis = new ArrayList();
        ArrayList directs = new ArrayList();
        while (it.hasNext()) {
            TransformTarget current = (TransformTarget) it.next();
            clis.add(new XslCliTest(current, report));
            directs.add(new XslDirectTest(current,report));
        }
        for (int i = 0; i < clis.size(); i++) {
            this.addTest((TestCase) clis.get(i));
        }
        for (int i = 0; i < directs.size(); i++) {
            this.addTest((TestCase) directs.get(i));
        }
    }

}