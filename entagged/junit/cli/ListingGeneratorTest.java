/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.cli;

import entagged.cli.ListingGenerator;

/**
 * Tests the {@link entagged.cli.ListingGenerator}.
 * 
 * @author Christian Laireiter
 */
public class ListingGeneratorTest extends CliBase {

    /**
     * Creates an instance.
     * @param suite
     */
    public ListingGeneratorTest(CliSuite suite) {
        super(suite,"ListingGenerator");
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#runTest()
     */
    protected void runTest() throws Throwable {
        ListingGenerator
                .main(new String[] { parent.getDirectory().getAbsolutePath(),
                        dst.getAbsolutePath() });
        assertFalse(this.dst.length() == 0);
    }
}