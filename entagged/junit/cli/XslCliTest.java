/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.cli;

import java.io.File;

import junit.framework.TestCase;
import entagged.cli.XslTransformer;
import entagged.listing.xml.TransformTarget;

/**
 * This test will use the main method of the cli interface.
 * 
 * @author Christian Laireiter
 */
public class XslCliTest extends TestCase {

    /**
     * The reportfile which should be transformed.
     */
    protected File reportFile;

    /**
     * The file the report is written to.
     */
    protected File transformed;

    /**
     * Transformation which should be tested.
     */
    protected TransformTarget transformTarget;

    /**
     * Creates the test.
     * 
     * @param target
     *                  The transformation
     * @param report
     *                  The file of the xml source.
     */
    public XslCliTest(TransformTarget target, File report) {
        super("Xsl XLI on: " + target.getType() + " in " + target.getLanguage());
        this.transformTarget = target;
        this.reportFile = report;
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#runTest()
     */
    protected void runTest() throws Throwable {
        XslTransformer.main(new String[] { this.transformTarget.getType(),
                this.transformTarget.getLanguage(),
                reportFile.getAbsolutePath(),
                this.transformed.getAbsolutePath() });
        assertTrue(this.transformed.exists() && this.transformed.isFile());
        assertTrue(this.transformed.length() > 0);
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        this.transformed = new File(this.reportFile.getParent(),
                "transformed.txt");
        //if (this.transformed.exists())
        //	this.transformed.delete();
    }
    
    /** (overridden)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        assertTrue(this.transformed.delete());
    }

}