/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.cli;

import java.io.File;

import entagged.junit.resource.Configuration;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * This suite should test all cli programs.
 * 
 * @author Christian Laireiter
 */
public class CliSuite extends TestSuite {

    private File directory;

    /**
     * Creates the tests.
     * 
     * @return
     */
    public static Test suite() {
        final CliSuite suite = new CliSuite(Configuration
                .getFile("junit.format.directory"));
        suite.addTest(new ListingGeneratorTest(suite));
        suite.addTest(new XmlListingGeneratorTest(suite));
        suite.addTest(XslSuite.suite());
        return suite;
    }

    /**
     * Creates an instance.
     * @param sourceDir
     *  
     */
    public CliSuite(File sourceDir) {
        super("CLI programs");
        this.directory = sourceDir;
    }

    /**
     * @return Returns the directory.
     */
    public File getDirectory() {
        return directory;
    }
}