/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import entagged.audioformats.AudioFileFilter;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

/**
 * Provides some basic tools
 * 
 * @author Christian Laireiter
 */
public class Utils {

    /**
     * This method creates a copy from <code>toCopy</code> preventing the file
     * extension and path.
     * 
     * @param toCopy
     *                  The file to be copied.
     * @return The copy of the file if successful.
     * @throws IOException
     *                   Read and write errors.
     */
    public static File createCopy(File toCopy) throws IOException {
        File result = null;
        //assertTrue("Invalid file name: " + absolute, extensionPos != -1);
        File parent = toCopy.getParentFile();
        result = new File(parent, "Copyof"
                + toCopy.getName().replaceAll(" ", "_"));
        //assertTrue(result.createNewFile());
        result.createNewFile();
        FileInputStream fis = new FileInputStream(toCopy);
        FileOutputStream fos = new FileOutputStream(result);
        byte[] buffer = new byte[8192];
        int read = 0;
        while ((read = fis.read(buffer)) > 0) {
            fos.write(buffer, 0, read);
        }
        fos.flush();
        fos.close();
        fis.close();
        return result;
    }
    
    public static File getTaggedFile(File directory) throws Exception {
        //File directory = Configuration.getFile("junit.format.directory");
        if (!(directory.exists() && directory.isDirectory() && directory
                .canRead())) {
            throw new AssertionFailedError(
                    "config.properties->junit.formats.directory should be a readable directory. ");
        }
        File[] files = directory.listFiles(new AudioFileFilter());
        for (int i = 0; i < files.length; i++) {
            File current = files[i];
            if (current.isFile() && current.canRead()) {
                return current;
            }
        }
		return null; // no file found
    }

    /**
     * @param file 
     * @return
     */
    public static String getNameNoExtension(File file) {
        String name = file.getName();
        String nameNoExt;
        int dotIndex = name.lastIndexOf('.');
        if (dotIndex > 0 && dotIndex < (name.length() - 1)) {
            nameNoExt = name.substring(0, dotIndex);
        } else {
            nameNoExt = "";
        }
        return nameNoExt;
    }
    
    /**
     * Creates a copy of the given directory.
     * 
     * @param toCopy
     *                  Directory to copy.
     * @return A copy of the given directory.
     * @throws IOException
     *                   errors.
     */
    public static File createCopyOfDirectory(File toCopy) throws IOException {
        File parent = toCopy.getParentFile();
        File copy = new File(parent, "Copy" + toCopy.getName());
        if (!copy.exists()) {

            Assert.assertTrue(
                    "Cannot create copy: " + toCopy.getAbsolutePath(), copy
                            .mkdir());
            File[] children = toCopy.listFiles();
            for (int i = 0; i < children.length; i++) {
                if (children[i].isFile()) {
                    Assert
                            .assertNotNull(createFileCopy(children[i], copy,
                                    null));
                }
            }
        }
        return copy;
    }

    /**
     * This method will copy <code>toCopy</code> into the directory
     * <code>destinationDir</code> and name it like <code>fileName</code>.
     * <br>
     * 
     * @param toCopy
     *                  Source
     * @param destinationDir
     *                  Directory where to place copy.
     * @param fileName
     *                  name of the copy. if <code>null</code>, original name will
     *                  be taken.
     * @return The copy.
     * @throws IOException
     *                   errors.
     */
    public static File createFileCopy(File toCopy, File destinationDir,
            String fileName) throws IOException {
        if (fileName == null) {
            fileName = toCopy.getName();
        }
        File copy = new File(destinationDir, fileName);
        Assert.assertTrue(copy.createNewFile());
        FileInputStream fis = new FileInputStream(toCopy);
        FileOutputStream fos = new FileOutputStream(copy);
        byte[] buffer = new byte[8192];
        int read = 0;
        while ((read = fis.read(buffer)) > 0) {
            fos.write(buffer, 0, read);
        }
        fos.flush();
        fos.close();
        fis.close();
        return copy;
    }

    /**
     * This method deletes all contents recursively and finally the directory
     * itself.
     * 
     * @param dir
     *                  Directory to delete.
     * @return <code>true</code> if successfull
     */
    public static boolean deleteDirectory(File dir) {
        boolean result = true;
        File[] files = dir.listFiles();
        for (int i = 0; i < files.length && result; i++) {
            if (files[i].isDirectory()) {
                result = deleteDirectory(files[i]);
            } else {
                result = files[i].delete();
            }
        }
        result = dir.delete();
        return result;
    }

    /**
     * This method reads all available data out of given input stream.
     * 
     * @param is
     *                  Stream to read from.
     * @return Number of bytes read.
     */
    public static int flushStream(InputStream is) {
        int result = 0;
        byte[] buf = new byte[8192];
        int read = -1;
        try {
            while (is.available() > 0 && (read = is.read(buf)) > 0)
                result += read;
        } catch (Exception e) {
            // Ignore
        }
        return result;
    }

    /**
     * Reads as much as is available from is and returns the data as a String.
     * 
     * @param is
     *                  to read From.
     * @return Read data interpreted as String.
     */
    public static String readFromStream(InputStream is) {
        StringBuffer result = new StringBuffer();
        int read = -1;
        byte[] buf = new byte[8192];
        try {
            while (is.available() > 0 && (read = is.read(buf)) > 0) {
                result.append(new String(buf, 0, read));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}