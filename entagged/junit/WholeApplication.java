/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit;

import java.io.File;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestSuite;
import entagged.junit.audioformats.DirectoryTest;
import entagged.junit.cli.CliSuite;
import entagged.junit.gui.options.OptionPanelTest;
import entagged.junit.resource.Configuration;
import entagged.junit.tageditor.tools.FileRenamerTestSuite;
import entagged.junit.tools.trackenumeration.TrackEnumerationTestSuite;
import entagged.tageditor.resources.LangageManager;
import entagged.tageditor.resources.PreferencesManager;

/**
 * A suite summarizing all subtests.
 * 
 * @author Christian Laireiter
 */
public class WholeApplication extends TestSuite {

    /**
     * This method will test if the configuration file has been adjusted to the
     * current enviroment (Computer).
     */
    private static void checkConfiguration() {
        String directroy = Configuration.getProperty("junit.format.directory");
        File file = new File(directroy);
        Assert
                .assertTrue(
                        "Please check \"entagged.junit.resource.config.properties\" for valid values",
                        file.exists() && file.isDirectory() && file.canRead());
    }

    /**
     * Creates a suite with all Tests.
     * 
     * @return A test suite.
     * @throws Throwable
     */
    public static Test suite() throws Throwable {
        checkConfiguration();
        PreferencesManager.initPreferences();
		LangageManager.initLangage();
        WholeApplication wa = new WholeApplication();
        wa.addTest(DirectoryTest.suite());
        wa.addTest(TrackEnumerationTestSuite.suite());
        wa.addTest(OptionPanelTest.suite());
        wa.addTest(CliSuite.suite());
		wa.addTest(FileRenamerTestSuite.suite());
        return wa;
    }

}