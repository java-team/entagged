/*
 * Created on Mar 29, 2005
 *
 */
package entagged.junit.tageditor.tools;

import java.io.File;
import entagged.junit.Utils;
import entagged.junit.resource.Configuration;
import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * @author sgilroy
 *
 */
public class FileRenamerTestSuite extends TestSuite {

    /**
     * The copy of the working directory.
     */
    protected File copyDir;

    protected File taggedFile;
    
	/**
     * Entry point for JUnit
     * 
     * @return A Testsuite
     * @throws Throwable
     *                    anything
     */
    public static Test suite() throws Throwable {
        final FileRenamerTestSuite suite = new FileRenamerTestSuite();
        final RenameFromTagTest renameFromTagTest = new RenameFromTagTest("testFileRenaming");
        suite.addTest(new TestCase("Setup") {
            protected void runTest() throws Throwable {
                suite.initialize();
                renameFromTagTest.taggedFile = suite.taggedFile;
            }
        });
        suite.addTest(renameFromTagTest);
        suite.addTest(new FileRenamerTest("testTransformations"));
        //suite.addTest(new OfFunctionTest(suite));
        //suite.addTest(new SaveRunTest(suite));
        suite.addTest(new TestCase("Delete") {
            protected void runTest() throws Throwable {
                Assert.assertTrue("Delete copy "
                        + suite.copyDir.getAbsolutePath(), Utils
                        .deleteDirectory(suite.copyDir));
            }
        });
        return suite;
    }

    /**
     * Creates an instance, wich will create a copy of the working direcotry.
     *  
     */
    public FileRenamerTestSuite() {
        super("FileRenamerTestSuite");
    }

    /**
     *  
     */
    public void initialize() {
        File dir = Configuration.getFile("junit.format.directory");
        try {
            this.copyDir = Utils.createCopyOfDirectory(dir);
            
    		this.taggedFile = Utils.getTaggedFile(this.copyDir);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(copyDir);
    }
}
