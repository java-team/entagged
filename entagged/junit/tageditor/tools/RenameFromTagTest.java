/*
 * Created on Mar 29, 2005
 *
 */
package entagged.junit.tageditor.tools;

import java.io.File;

import junit.framework.TestCase;

/**
 * @author sgilroy
 *
 */
public class RenameFromTagTest extends TestCase {

    protected File taggedFile;

    /**
     * @param string
     * @param taggedFile
     */
    public RenameFromTagTest(String testName) {
        super(testName);
    }

    public void testFileRenaming() throws Throwable{
		/*
		 * To all readers, especially to 'sgilroy',
		 * I (Christian Laireiter) commented the contents of the method, because I created
		 * a complete new renaming, with preview. For now it isn't possible
		 * to rename a file directly, any more.
		 * The user must confirm the changes. And I'll think of an automated way
		 * of testing later.
		 */
//    	AudioFile af = AudioFileIO.read(taggedFile);
//    	Assert.assertNotNull(af);
//        Tag tag = af.getTag();
//        Assert.assertNotNull(tag);
//        tag.setArtist("Test Artist");
//        tag.setTitle("Test Title");
//        af.commit();
//    	
//    	FileRenamer fr = new FileRenamer();
//    	fr.setDirectoryPattern(af.getParent());
//    	
//        fr.setFilenamePattern(FileRenamer.ARTIST_MASK + " - " + FileRenamer.TITLE_MASK);
//        taggedFile = fr.renameFromTag(af);
//        af = AudioFileIO.read(taggedFile);
//        
//        String newNameNoExt = Utils.getNameNoExtension(taggedFile);
//        
//        Assert.assertEquals(newNameNoExt, "Test Artist - Test Title");
//        Assert.assertEquals(taggedFile.getPath(), af.getPath());
//
//        // if we rename to the same name again, we should have no problem
//        taggedFile = fr.renameFromTag(af);
//        af = AudioFileIO.read(taggedFile);
//
//        tag = af.getTag();
//        
//        // now test changing case
//        tag.setArtist("test artist");
//        af.commit();
//        taggedFile = fr.renameFromTag(af);
//        af = AudioFileIO.read(taggedFile);
//        
//        newNameNoExt = Utils.getNameNoExtension(taggedFile);
//        Assert.assertEquals(newNameNoExt, "test artist - Test Title");
//        
//        // trying to rename a file to an existing name should throw an exception
//        File fileCopy = Utils.createCopy(taggedFile);
//        af = AudioFileIO.read(fileCopy);
//        boolean exceptionThrown = false;
//        
//        LangageManager.initLangage();
//        try {
//            fileCopy = fr.renameFromTag(af);
//        }
//        catch (OperationException e) {
//            // FIXME: how do we know we have the right exception?
//            exceptionThrown = true;
//        }
//        Assert.assertTrue(exceptionThrown);
//        Assert.assertFalse(fileCopy.getPath().equals(taggedFile.getPath()));
//        Assert.assertTrue(fileCopy.exists());
//        Assert.assertTrue(taggedFile.exists());
    }
}
