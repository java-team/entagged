/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.tageditor.tools;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import entagged.audioformats.generic.GenericTag;
import entagged.tageditor.tools.stringtransform.TransformOperation;
import entagged.tageditor.tools.stringtransform.TransformSet;
import entagged.tageditor.tools.stringtransform.operations.CapitalizeOp;
import entagged.tageditor.tools.stringtransform.operations.StripNonUsASCIIOp;

/**
 * @author Miguel Sahag�n (Anacleto)
 *
 */
public class FileRenamerTest extends TestCase {
	public  FileRenamerTest(String name){
		super(name);
	}
	public void testTransformations() throws Throwable {
		GenericTag gt = new GenericTag();
		TransformSet transformSet = new TransformSet(new TransformOperation[] {
				new CapitalizeOp(),
				new StripNonUsASCIIOp()
		});

		String tst1="Hi this (is) the xiii ��";
		String tstr1="Hi This (Is) The Xiii An";
		gt.setComment(tst1);
		
		transformSet.transformFirstCommons(gt);
		
		System.out.println(gt.getFirstComment());
		
		assertEquals(tstr1,gt.getFirstComment());
	}
	
	public static Test suite() {
			return new TestSuite(FileRenamerTest.class);
		}
	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());
	}
}
