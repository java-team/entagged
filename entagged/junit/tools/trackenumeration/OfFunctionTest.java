/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.tools.trackenumeration;

import java.io.File;

import junit.framework.TestCase;
import entagged.audioformats.AudioFileFilter;
import entagged.audioformats.AudioFileIO;
import entagged.tageditor.tools.TrackEnumerator;
import entagged.tageditor.tools.TrackEnumeratorSettings;

/**
 *	This method will verify the proper implementation of the "Of"-Function. 
 *
 * @author Christian Laireiter
 */
public class OfFunctionTest extends TestCase {

    /**
     * The suite which created this test.
     */
    private TrackEnumerationTestSuite parent;

    /**
     * Creates the test.
     * @param suite
     */
    public OfFunctionTest (TrackEnumerationTestSuite suite) {
        super("OfFunctionTest");
        this.parent = suite;
    }
    
    /** (overridden)
     * @see junit.framework.TestCase#runTest()
     */
    protected void runTest() throws Throwable {
        TrackEnumerator te = new TrackEnumerator();
        TrackEnumeratorSettings settings = te.getSettings();
        settings.setOfString(" of <n>");
        settings.setFiles(new File[] {parent.copyDir});
        String of = " of "+String.valueOf(te.process(false));
        File[] files = parent.copyDir.listFiles(new AudioFileFilter());
        for (int i = 0; i < files.length; i++) {
            String track = AudioFileIO.read(files[i]).getTag().getFirstTrack();
            assertTrue ("End of track doesn't match: ",track.endsWith(of));
        }
    }
}
