/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.tools.trackenumeration;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import entagged.junit.Utils;
import entagged.junit.resource.Configuration;

/**
 * This class will setup a copy of the configured working directory and create
 * some tests for validating the
 * {@link entagged.tageditor.tools.TrackEnumerator}.<br>
 * 
 * @author Christian Laireiter
 */
public class TrackEnumerationTestSuite extends TestSuite {

    /**
     * Creates the tests.
     * 
     * @return
     */
    public static Test suite() {
        final TrackEnumerationTestSuite suite = new TrackEnumerationTestSuite();
        final RandomSelectionTest test = new RandomSelectionTest("Random",
                suite.copyDir);
        suite.addTest(new TestCase("Setup") {
            protected void runTest() throws Throwable {
                suite.initialize();
                test.dir = suite.copyDir;
            }
        });
        suite.addTest(test);
        suite.addTest(new OfFunctionTest(suite));
        suite.addTest(new SaveRunTest(suite));
        suite.addTest(new TestCase("Delete") {
            protected void runTest() throws Throwable {
                Assert.assertTrue("Delete copy "
                        + suite.copyDir.getAbsolutePath(), Utils
                        .deleteDirectory(suite.copyDir));
            }
        });
        return suite;
    }

    /**
     * The copy of the working directory.
     */
    protected File copyDir;

    /**
     * Creates an instance, wich will create a copy of the working direcotry.
     *  
     */
    public TrackEnumerationTestSuite() {
        super("TrackEnumerationTestSuite");
    }

    /**
     *  
     */
    public void initialize() {
        File dir = Configuration.getFile("junit.format.directory");
        try {
            this.copyDir = Utils.createCopyOfDirectory(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(copyDir);
    }

}