/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.tools.trackenumeration;

import java.io.File;

import entagged.audioformats.AudioFile;
import entagged.audioformats.AudioFileFilter;
import entagged.audioformats.AudioFileIO;
import entagged.tageditor.tools.TrackEnumerator;

import junit.framework.TestCase;

/**
 * This Test checks whether the saveRun method works.
 * 
 * @author Christian Laireiter
 */
public class SaveRunTest extends TestCase {

    private TrackEnumerationTestSuite parent;

    /**
     * Creates an instance.
     * @param suite
     */
    public SaveRunTest(TrackEnumerationTestSuite suite) {
        super("SaveRunTest");
        this.parent = suite;
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#runTest()
     */
    protected void runTest() throws Throwable {
        File[] files = parent.copyDir.listFiles(new AudioFileFilter());
        for (int i = 0; i < files.length; i++) {
            AudioFile file = AudioFileIO.read(files[i]);
            file.getTag().setTrack("do not modify");
            AudioFileIO.write(file);
        }
        TrackEnumerator te = new TrackEnumerator();
        te.getSettings().setSaveRun(true);
        te.getSettings().setFiles(new File[] {parent.copyDir});
        File cantwork = new File(parent.copyDir, "CantWork.ogg");
        assertTrue(cantwork.createNewFile());
        int processed = te.process(false);
        assertTrue(processed < 0);
        for (int i = 0; i < files.length; i++) {
            AudioFile file = AudioFileIO.read(files[i]);
            assertTrue("Tag written despite errornous file.", file.getTag()
                    .getFirstTrack().equals("do not modify"));
        }
        assertTrue(cantwork.delete());
    }
}