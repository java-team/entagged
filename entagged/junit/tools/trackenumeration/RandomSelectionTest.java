/*
 *  ********************************************************************   **
 *  Copyright notice                                                       **
 *  **																	   **
 *  (c) 2003 Entagged Developpement Team				                   **
 *  http://www.sourceforge.net/projects/entagged                           **
 *  **																	   **
 *  All rights reserved                                                    **
 *  **																	   **
 *  This script is part of the Entagged project. The Entagged 			   **
 *  project is free software; you can redistribute it and/or modify        **
 *  it under the terms of the GNU General Public License as published by   **
 *  the Free Software Foundation; either version 2 of the License, or      **
 *  (at your option) any later version.                                    **
 *  **																	   **
 *  The GNU General Public License can be found at                         **
 *  http://www.gnu.org/copyleft/gpl.html.                                  **
 *  **																	   **
 *  This copyright notice MUST APPEAR in all copies of the file!           **
 *  ********************************************************************
 */
package entagged.junit.tools.trackenumeration;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.Random;

import entagged.audioformats.AudioFileFilter;
import entagged.audioformats.AudioFileIO;
import entagged.tageditor.tools.TrackEnumerator;
import entagged.tageditor.tools.TrackEnumeratorSettings;

import junit.framework.TestCase;

/**
 * This will do a rondom selection of the files of the given directory and let
 * the tracks be enumerated.
 * 
 * @author Christian Laireiter
 */
public class RandomSelectionTest extends TestCase {

    /**
     * The directory to test.
     */
    protected File dir;

    /**
     * The files to enumerate.
     */
    private File[] files;

    /**
     * The indices of {@link #files}. In that order the files should be
     * enumerated.
     */
    private int[] selIndexes;

    /**
     * The enumerator instance.
     */
    private TrackEnumerator enumerator;

    /**
     * Constructor for RandomSelectionTest.
     * 
     * @param name
     *                  name
     * @param directory
     *                  Directory to test.
     */
    public RandomSelectionTest(String name, File directory) {
        super(name);
        this.dir = directory;
    }

    /**
     * This method will create an array of indexes for accessing files. Each
     * index will be supplied once.
     * 
     * @param objects
     *                  Array to create the access list of.
     * @return array with indices for files.
     */
    private int[] createSelection(Object[] objects) {
        Random rand = new Random(System.currentTimeMillis());
        int[] result = new int[objects.length];
        HashSet used = new HashSet();
        for (int i = 0; i < result.length; i++) {
            boolean assigned = false;
            while (!assigned) {
                int index = rand.nextInt();
                if (index < 0)
                    index *= -1;
                index %= objects.length;
                if (!used.contains(new Integer(index))) {
                    used.add(new Integer(index));
                    result[i] = index;
                    assigned = true;
                }
            }
        }
        return result;
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        files = dir.listFiles(new FileFilter() {
            AudioFileFilter filter =new AudioFileFilter();
            public boolean accept(File pathname) {
                return pathname.isFile() && filter.accept(pathname);
            }
        });
        selIndexes = createSelection(files);
        File[] selection = new File[files.length];
        for (int i = 0; i < selection.length; i++) {
            selection[i] = files[selIndexes[i]];
        }
        this.enumerator = new TrackEnumerator();
        TrackEnumeratorSettings settings = enumerator.getSettings();
        settings.setFiles(selection);
        settings.setDigitCount(0);
        settings.setDynamicDigitExtend(false);
        settings.setOfString(null);
    }

    /**
     * (overridden)
     * 
     * @see junit.framework.TestCase#runTest()
     */
    protected void runTest() throws Throwable {
        int processedCount = enumerator.process(false);
        assertEquals(files.length, processedCount);
        for (int i = 0; i < selIndexes.length; i++) {
            File current = files[selIndexes[i]];
            String track = AudioFileIO.read(current).getTag().getFirstTrack();
            assertEquals("Track value unexpected", String.valueOf(i + 1), track);
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}